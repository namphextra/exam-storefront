import { Settings } from 'react-slick';

export interface StarData {
  percentage: number;
  point?: number;
  size?: 'small' | 'large' | 'inherit' | 'default';
}

export interface GroupCer {
  title: string;
  sliderSettings?: Settings;
  className?: string;
}
