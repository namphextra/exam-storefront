import React from 'react';
import { Typography, Container } from '@material-ui/core';
import { GroupCer } from './types';
import Slider, { Settings } from 'react-slick';
import { Star } from '@/components/StarRating';

const sliderSettings: Settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false
};

const certificates = [
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/Tech-news_Hero_1260x600-960x600.jpg',
    title: 'Cer1',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  },
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/AdobeStock_98976335-1024x658-960x658.jpeg',
    title: 'Cer2',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  },
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/PM-news-autonomous-0718.png',
    title: 'Cer3',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  },
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/Tech-news_Hero_1260x600-960x600.jpg',
    title: 'Cer4',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  },
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/Tech-news_Hero_1260x600-960x600.jpg',
    title: 'Cer5',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  },
  {
    banner: 'https://strategicfocus.com/wp-content/uploads/2018/12/Tech-news_Hero_1260x600-960x600.jpg',
    title: 'Cer6',
    price: '$250',
    comparePrice: '$350',
    ratingPercentage: 87,
    ratingCount: 344
  }
];

export function GroupCertification(props: GroupCer): JSX.Element {
  const settings = props.sliderSettings ? props.sliderSettings : { ...sliderSettings };
  return (
    <div className={'group-cer__container ' + props.className}>
      <Container>
        <div className={'mb-8'}>
          <Typography variant={'h5'}>{props.title}</Typography>
        </div>
        <Slider {...settings}>
          {certificates.map(
            (cer, i): JSX.Element => {
              return (
                <div className={'certificate__container px-4 py-2'} key={i}>
                  <div className={'certificate bg-white rounded-lg shadow'}>
                    <div className="banner h-36">
                      <div className="bg-hover text-white text-lg flex items-center justify-center">{cer.title}</div>
                      <img className={'inline-block mb-3 w-full h-full object-cover rounded-t-lg'} src={cer.banner} alt={'best'} />
                    </div>
                    <div className="cer-content p-3">
                      <Typography variant={'h5'}>{cer.title}</Typography>
                      <div className={'flex items-center my-3'}>
                        <span className={'text-sm line-through color-gray-400 mr-2'}>{cer.comparePrice}</span>
                        <span className={'text-red-500 text-xl font-bold'}>{cer.price}</span>
                      </div>
                      <div className="flex justify-between items-center">
                        <Star percentage={4.6} point={4.6} size={'small'} />
                        <span className={'text-sm'}>{cer.ratingCount} ratings</span>
                      </div>
                    </div>
                  </div>
                </div>
              );
            }
          )}
        </Slider>
      </Container>
    </div>
  );
}
