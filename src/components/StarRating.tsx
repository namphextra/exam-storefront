import { StarRounded } from '@material-ui/icons';
import React from 'react';
import { StarData } from './types';

export const Star = (props: StarData): JSX.Element => {
  return (
    <div className="stars__container relative inline-block">
      {Array.from(new Array(5)).map((v, i) => {
        return (
          <span className="star color-gray-200 inline-block leading-none" key={i}>
            <StarRounded fontSize={props.size} />
          </span>
        );
      })}
      <div
        className="stars-main__container absolute whitespace-nowrap overflow-hidden inset-0"
        style={{ width: `${(props.point * 100) / 5}%` }}
      >
        {Array.from(new Array(5)).map((v, i) => {
          return (
            <span className="star inline-block leading-none" key={i}>
              <StarRounded color={'secondary'} fontSize={props.size} />
            </span>
          );
        })}
      </div>
    </div>
  );
};
