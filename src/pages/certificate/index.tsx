import React, { useEffect } from 'react';
import { Typography, Container, Grid, Button } from '@material-ui/core';
import { Rating } from './Rating';
import { GroupCertification } from '@/components/GroupCertification';
import { Settings } from 'react-slick';

const sliderSettings: Settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  autoplay: false
};

const popularExams = [
  {
    image: 'https://via.placeholder.com/150',
    name: 'cer1',
    price: '$100',
    comparePrice: '$200'
  },
  {
    image: 'https://via.placeholder.com/150',
    name: 'cer2',
    price: '$100',
    comparePrice: '$200'
  },
  {
    image: 'https://via.placeholder.com/150',
    name: 'cer3',
    price: '$100',
    comparePrice: '$200'
  }
];

export function Certificate(): JSX.Element {
  useEffect(() => {
    const el: HTMLElement = document.querySelector('.promote-grid');
    const topAnchor = el.offsetTop;
    const callback = () => {
      if (window.pageYOffset >= topAnchor - 16) {
        el.setAttribute('style', `top: 16px; left: ${el.offsetLeft}px; width: ${el.offsetWidth}px; position: fixed`);
      } else {
        el.removeAttribute('style');
      }
    };
    window.addEventListener('scroll', callback, false);
    return () => {
      window.removeEventListener('scroll', callback);
    };
  }, []);
  return (
    <div className="certificate__container pt-10">
      <Container>
        <Grid container spacing={3}>
          <Grid item sm={9}>
            <GroupCertification title={'Hot Certificates'} sliderSettings={sliderSettings} />
            <div className="description__container certificate__section shadow mt-10 p-6">
              <div className="mb-2">
                <Typography variant={'h4'}>Certification</Typography>
              </div>
              <span className={'text-sm line-through color-gray-400 mr-3'}>$350</span>
              <span className={'text-red-500 text-2xl font-bold'}>$250</span>
              <div className="subtitle mt-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc efficitur est non lectus auctor luctus. Etiam elementum mattis
                ornare. Ut at facilisis tellus. Maecenas egestas quam dignissim ante faucibus, non posuere orci mattis
              </div>
              <div className="actions mt-4 text-right">
                <Button color={'primary'} variant={'contained'} size={'large'}>
                  Buy now
                </Button>
              </div>
            </div>
            <Rating />
          </Grid>
          <Grid item sm={3} className={'promote-grid'}>
            <div className="promote-exams shadow">
              <div className="popular-exams__container">
                {popularExams.map(
                  (cer, i): JSX.Element => {
                    return (
                      <div className={'popular-exam__container flex flex-col items-center mb-6'} key={i}>
                        <img src={cer.image} alt="" />
                        <p className={'mt-4 mb-2'}>{cer.name}</p>
                        <span className={'text-sm line-through color-gray-400'}>{cer.comparePrice}</span>
                        <span className={'text-red-500 text-2xl font-bold'}>{cer.price}</span>
                      </div>
                    );
                  }
                )}
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
