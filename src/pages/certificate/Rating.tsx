import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { StarOutlineRounded } from '@material-ui/icons';
import { Star } from '@/components/StarRating';
import { StarData } from '@/components/types';

const starsData: Array<StarData> = [
  {
    percentage: 76
  },
  {
    percentage: 17
  },
  {
    percentage: 4
  },
  {
    percentage: 1
  },
  {
    percentage: 2
  }
];

const StarComp = (i: number, starData: StarData): JSX.Element => {
  return (
    <div className="star__container flex items-center" key={i}>
      <span>{i}</span>
      <StarOutlineRounded color={'secondary'} />
      <div className="progress relative flex-1 bg-gray-200 rounded-3xl h-2 mx-3">
        <div className="processing absolute inset-0 bg-secondary rounded-3xl h-full" style={{ width: `${starData.percentage}%` }} />
      </div>
      <div className="percentage w-9">{starData.percentage}%</div>
    </div>
  );
};

const Reviewer = (i: number): JSX.Element => {
  return (
    <div className={'reviewer__container mt-10'} key={i}>
      <div className="reviewer-info__container flex justify-between">
        <div className="reviewer-info flex items-center">
          <div className="avatar rounded-full mr-3">
            <img src="https://graph.facebook.com/v2.10/3287813824568998/picture" alt="reviewer" className={'rounded-full'} />
          </div>
          <div className="reviewer-name-rating">
            <div className="reviewer-name">Carol</div>
            <div className="reviewer-rating">
              <Star percentage={5} size={'small'} />
            </div>
          </div>
        </div>
        <div className="reviewer-time text-gray-400 text-sm">3 months ago</div>
      </div>
      <div className="reviewer-content text-gray-400 mt-4">No comment</div>
    </div>
  );
};

export function Rating(): JSX.Element {
  return (
    <div className={'rating__container certificate__section shadow mt-4 p-6'}>
      <div className={'mb-8'}>
        <Typography variant={'h5'}>Rating</Typography>
      </div>
      <Grid container className={'flex'}>
        <Grid item sm={5} className={'p-4 pr-10 text-center rating-grid'}>
          <div className="point text-7xl">4.6</div>
          <Star percentage={4.6} point={4.6} size={'large'} />
          <p className={'text-sm mt-2'}>(1078 peoples reviewed)</p>
          <div className="stars-ranking__container mt-14">{starsData.map((v, i): JSX.Element => StarComp(starsData.length - i, v))}</div>
        </Grid>
        <Grid item sm={7} className={'p-4 pl-10'}>
          {Array.from(new Array(5)).map((v, i): JSX.Element => Reviewer(i))}
        </Grid>
      </Grid>
    </div>
  );
}
