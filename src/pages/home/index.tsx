import React from 'react';
import { Container, Typography, Grid } from '@material-ui/core';
import { CreateOutlined as CreateIcon, BeenhereOutlined, UpdateOutlined, LiveHelpOutlined } from '@material-ui/icons';
import { GroupCertification } from '@/components/GroupCertification';

export function Home(): JSX.Element {
  return (
    <div className="home__container pt-4">
      <Container>
        <GroupCertification title={'Hot Certificates'} className={'pt-10'} />
      </Container>
      <div className={'features mt-10'}>
        <Container className={'py-8 relative'}>
          <img
            className={'absolute'}
            style={{ maxWidth: '178px', top: 0, transform: 'rotate(180deg)', right: '20%' }}
            src="https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://s3.amazonaws.com/coursera_assets/front-page-story/secondary-consumer-cta/red-dotted-half-circle.png?auto=format%2Ccompress&dpr=1&w=178&h=90&q=40"
            alt=""
          />
          <img
            className={'absolute'}
            style={{ maxWidth: '157px', bottom: 0, left: '30%' }}
            src="https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://s3.amazonaws.com/coursera_assets/front-page-story/achieve-your-goals/blue-dotted-blob-half.png?auto=format%2Ccompress&dpr=1&w=157&h=64&q=40"
            alt=""
          />
          <div className={'mb-8'}>
            <Typography variant={'h5'}>Archive your goal with Vietdumps</Typography>
          </div>
          <Grid container>
            <Grid item sm={3} className={'text-center px-4'}>
              <CreateIcon color={'primary'} fontSize={'large'} />
              <div className={'real-question font-bold mb-3 mt-2'}>Real Cisco Exam Questions</div>
              <div className={''}>100% Real Cisco Exam Dumps with 100% Verified Answers.</div>
            </Grid>
            <Grid item sm={3} className={'text-center px-4'}>
              <BeenhereOutlined color={'primary'} fontSize={'large'} />
              <div className={'real-question font-bold mb-3 mt-2'}>100% Pass Rate Guarantee</div>
              <div className={''}>
                Vietdumps has trusted IT teams and Real Cisco Exam Answers and Questions, which guarantees you get certification in one
                goes.
              </div>
            </Grid>
            <Grid item sm={3} className={'text-center px-4'}>
              <UpdateOutlined color={'primary'} fontSize={'large'} />
              <div className={'real-question font-bold mb-3 mt-2'}>Free & Timely Update Exam Dumps</div>
              <div className={''}>
                Once Cisco exam dumps change, we will timely inform you so that you can pass your cisco exams successfully.
              </div>
            </Grid>
            <Grid item sm={3} className={'text-center px-4'}>
              <LiveHelpOutlined color={'primary'} fontSize={'large'} />
              <div className={'real-question font-bold mb-3 mt-2'}>7/24 Customers Service Support</div>
              <div className={''}>Vietdump’s customer service keep online to to help all candidates resolve your problems.</div>
            </Grid>
          </Grid>
        </Container>
      </div>
      <GroupCertification title={'Group 1'} className={'pt-10'} />
    </div>
  );
}
