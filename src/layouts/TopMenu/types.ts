interface PropsAuthenticate {
  open: boolean;
  readonly setOpen: (value: boolean) => void;
  readonly typeModal: string;
  readonly setTypeModal: (value: string) => void;
}

export { PropsAuthenticate };
