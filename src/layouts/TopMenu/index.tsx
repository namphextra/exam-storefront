import React, { useState } from 'react';
import { Container, Grid, Button, TextField, InputAdornment } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import Logo from '@assets/images/icons/logo.png';
import { NavLink } from 'react-router-dom';
import SignIn from '@/authenticate/AuthModal';

const useStyles = makeStyles({
  root: {
    textTransform: 'none'
  },
  buttonSignUp: {
    textAlign: 'right',
    paddingRight: '8px'
  },
  firstRow: {
    position: 'relative',
    '&::after': {
      content: '""',
      position: 'absolute',
      bottom: '0',
      left: '0',
      width: '100%',
      height: '1px',
      boxShadow: '0 1px rgba(64,87,109,.07)'
    }
  }
});

const Index = (): JSX.Element => {
  const classes = useStyles();
  const [openAuthModal, setOpenAuthModal] = useState(false);
  const [typeModal, setTypeModal] = useState('sign-in');

  return (
    <div className="menu-top__container">
      {openAuthModal && (
        <SignIn
          open={openAuthModal}
          setOpen={(value: boolean) => setOpenAuthModal(value)}
          typeModal={typeModal}
          setTypeModal={(val) => setTypeModal(val)}
        />
      )}
      <Container fixed>
        <Grid container classes={{ root: classes.firstRow }} className="py-3 justify-items-center">
          <Grid item sm={2}>
            <img src={Logo} alt="vietdumps" />
          </Grid>
          <Grid item sm={7}>
            <TextField
              placeholder="What would you like to find?"
              variant="outlined"
              size={'small'}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                )
              }}
            />
          </Grid>
          <Grid item sm={2} classes={{ root: classes.buttonSignUp }} className="flex items-center justify-end">
            <Button
              color="primary"
              variant="contained"
              classes={{ label: classes.root }}
              onClick={() => {
                setTypeModal('sign-up');
                setOpenAuthModal(true);
              }}
            >
              Sign up
            </Button>
          </Grid>
          <Grid item sm={1} className="flex items-center justify-start">
            <Button
              classes={{ label: classes.root }}
              onClick={() => {
                setTypeModal('sign-in');
                setOpenAuthModal(true);
              }}
            >
              Login
            </Button>
          </Grid>
        </Grid>
        <Grid container className="list-menu">
          <Grid item sm={1}>
            <NavLink to="/" exact>
              Home
            </NavLink>
          </Grid>
          <Grid item sm={2}>
            <NavLink to="/certificate">Certificate1</NavLink>
          </Grid>
          <Grid item sm={2}>
            <NavLink to="/certificate1">Certificate1</NavLink>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Index;
