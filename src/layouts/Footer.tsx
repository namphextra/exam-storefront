import React from 'react';
import { Container, Grid, Link } from '@material-ui/core';
import { Facebook, WhatsApp } from '@material-ui/icons';

const Footer = (): JSX.Element => {
  return (
    <div className={'footer__container py-10 bg-white mt-10'}>
      <Container>
        <Grid container spacing={6}>
          <Grid item sm={6} className={'text-right'}>
            <p>© 2020 Vietdumps Inc. All rights reserved.</p>
            <a href="#">
              <Facebook fontSize={'large'} style={{ color: '#38529a' }} className={'mx-2'} />
            </a>
            <a href="#">
              <WhatsApp fontSize={'large'} style={{ color: '#24cc63' }} />
            </a>
          </Grid>
          <Grid item sm={6}>
            <Link color={'inherit'} href="#" className={'block'}>
              Contact us
            </Link>
            <Link color={'inherit'} href="#" className={'block'}>
              About us
            </Link>
            <Link color={'inherit'} href="#" className={'block'}>
              FAQ
            </Link>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Footer;
