import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentLeft: {
    background: 'rgb(0, 35, 51)',
    width: '284px',
    color: 'white',
    borderRadius: '7px 0 0 7px'
  },
  contentRight: {
    background: 'white',
    width: '384px',
    borderRadius: '0 7px 7px 0'
  },
  content: {
    position: 'relative',
    '&:focus': {
      outline: 0
    }
  },
  closeModal: {
    position: 'absolute',
    top: '12px',
    right: '12px',
    cursor: 'pointer'
  },
  quote: {
    width: '100px',
    height: '8px',
    background: theme.palette.primary.main,
    borderRadius: '12px'
  }
}));

export default useStyles;
