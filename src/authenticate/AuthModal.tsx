import React, { useState, Fragment } from 'react';
import { PropsAuthenticate } from '@/layouts/TopMenu/types';
import { Modal, Fade, Backdrop, Typography, TextField, Button, FormControlLabel, Checkbox, Link } from '@material-ui/core';
import { CloseOutlined } from '@material-ui/icons';
import useStyles from '@/authenticate/styles';
import clsx from 'clsx';

const allMessages = {
  'sign-in': {
    title: 'Welcome Back to Vietdumps',
    subTitle: 'Sign in to continue to your account.'
  },
  'sign-up': {
    title: 'Join Vietdumps for Free',
    subTitle: 'Test your skill with exams.'
  }
};

const SignUp = (props: any): JSX.Element => {
  return (
    <Fragment>
      <form noValidate autoComplete={'off'} className={'mt-6'}>
        <div className="mb-6 flex justify-between">
          <div className="mr-3 w-1/2">
            <TextField
              variant={'outlined'}
              size={'small'}
              label={'First name'}
              value={props.firstName}
              onChange={(e) => props.setFirstName(e.target.value)}
            />
          </div>
          <TextField
            variant={'outlined'}
            className={'w-1/2'}
            size={'small'}
            label={'Last name'}
            value={props.lastName}
            onChange={(e) => props.setLastName(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <TextField
            variant={'outlined'}
            className={'w-full'}
            size={'small'}
            label={'Email address'}
            value={props.emailAddress}
            onChange={(e) => props.setEmailAddress(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <TextField
            variant={'outlined'}
            className={'w-full'}
            size={'small'}
            label={'Password'}
            type={'password'}
            value={props.password}
            onChange={(e) => props.setPassword(e.target.value)}
          />
        </div>
        <div className="w-full">
          <Button color={'primary'} variant={'contained'} className={'w-full'}>
            Sign Up
          </Button>
        </div>
      </form>
      <p className={'text-center mt-8'}>
        Already a member?{' '}
        <Link href={'#'} onClick={() => props.setTypeModal('sign-in')}>
          Sign In
        </Link>
      </p>
    </Fragment>
  );
};

const SignIn = (props: any): JSX.Element => {
  return (
    <Fragment>
      <form noValidate autoComplete={'off'} className={'mt-6'}>
        <div className="mb-6">
          <TextField
            variant={'outlined'}
            className={'w-full'}
            size={'small'}
            label={'Email address'}
            value={props.emailAddress}
            onChange={(e) => props.setEmailAddress(e.target.value)}
          />
        </div>
        <TextField
          className={'w-full'}
          variant={'outlined'}
          size={'small'}
          label={'Password'}
          value={props.password}
          onChange={(e) => props.setPassword(e.target.value)}
          type={'password'}
        />
        <FormControlLabel
          className={'my-4'}
          control={<Checkbox color={'primary'} checked={props.keepSignIn} onChange={(e) => props.setKeepSignIn(e.target.checked)} />}
          label={'Keep me signed in until I sign out'}
        />
        <div className="w-full">
          <Button color={'primary'} variant={'contained'} className={'w-full'}>
            Sign In
          </Button>
        </div>
      </form>
      <div className="mt-4">
        <Link href={'/reset-password'}>Forgot password?</Link>
      </div>
      <p className={'text-center mt-8'}>
        Not a member yet?{' '}
        <Link href={'#'} onClick={() => props.setTypeModal('sign-up')}>
          Sign Up
        </Link>
      </p>
    </Fragment>
  );
};

const AuthModal = (props: PropsAuthenticate): JSX.Element => {
  const classes = useStyles();
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [keepSignIn, setKeepSignIn] = useState(false);
  const onCloseModal = (): void => props.setOpen(false);
  const messages = allMessages[props.typeModal];
  return (
    <Modal
      className={clsx(classes.modal, 'sign-in__container')}
      open={props.open}
      BackdropComponent={Backdrop}
      BackdropProps={{ timeout: 300 }}
      aria-labelledby={'transition-modal-title'}
      aria-describedby={'transition-modal-description'}
      onClose={onCloseModal}
    >
      <Fade in={props.open}>
        <div className={clsx(classes.content, 'content flex')}>
          <CloseOutlined fontSize={'large'} onClick={onCloseModal} className={classes.closeModal} />
          <div className={clsx(classes.contentLeft, 'content-left p-6 flex flex-col justify-center')}>
            <Typography variant={'h4'}>{messages.title}</Typography>
            <div className={clsx(classes.quote, 'my-6')} />
            <p>{messages.subTitle}</p>
          </div>
          <div className={clsx(classes.contentRight, 'content-right px-8 pt-12 pb-8')}>
            {props.typeModal === 'sign-in'
              ? SignIn({
                  emailAddress,
                  setEmailAddress,
                  password,
                  setPassword,
                  keepSignIn,
                  setKeepSignIn,
                  setTypeModal: props.setTypeModal
                })
              : SignUp({
                  emailAddress,
                  setEmailAddress,
                  password,
                  setPassword,
                  firstName,
                  setFirstName,
                  lastName,
                  setLastName,
                  setTypeModal: props.setTypeModal
                })}
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default AuthModal;
