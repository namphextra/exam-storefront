import React from 'react';
import { render } from 'react-dom';
import TopMenu from '@/layouts/TopMenu';
import Footer from '@/layouts/Footer';
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from '@/pages/home';
import { Certificate } from '@/pages/certificate';
import '@assets/styles/app.scss';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#36c5f1',
      contrastText: '#fff'
    },
    secondary: {
      main: '#f9d641'
    }
  }
});

function App(): JSX.Element {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <TopMenu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/certificate" component={Certificate} />
          <Route path="/certificate1" component={Certificate} />
        </Switch>
        <Footer />
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;

if (document.getElementById('app')) {
  render(<App />, document.getElementById('app'));
}
