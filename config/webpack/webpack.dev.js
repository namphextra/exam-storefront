const { merge } = require('webpack-merge');
const common = require('./webpack.common');
const paths = require('../paths');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: paths.appDist,
    historyApiFallback: true,
    compress: true,
    hot: true,
    port: 4000,
    host: 'localhost'
  }
});
