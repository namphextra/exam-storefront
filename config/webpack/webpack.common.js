const paths = require('../paths');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: [paths.appIndexJs, paths.appIndexStyle],
  output: {
    filename: '[name].[hash].js',
    path: paths.appDist
  },
  resolve: {
    alias: {
      '@': paths.appSrc,
      '@assets': paths.appAssets
    },
    extensions: ['*', '.js', '.jsx', '.ts', '.tsx', '.png', '.jpg', '.svg', '.jpeg', '.gif', '.css']
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      inject: true,
      title: 'Vietdumps',
      scriptLoading: 'defer',
      template: paths.appSrc + '/index.html',
      favicon: paths.appAssets + '/images/icons/favicon.png'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].bundle.css',
      chunkFilename: '[id].css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.s[ca]ss?$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: false
            }
          },
          {
            loader: 'css-loader',
            options: {
              modules: false,
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [require('autoprefixer'), require('tailwindcss')]
              }
            }
          },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.(eot|ttf|woff|svg|png|jpe?g|gif)$/i,
        use: [{ loader: 'file-loader' }]
      }
    ]
  },
  optimization: {
    minimizer: [
      new CssMinimizerPlugin(),
      new TerserPlugin({
        parallel: true
      })
    ]
  },
  externals: {
    tailwindcss: ['https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css', '_', 'head']
  }
};
