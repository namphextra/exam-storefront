const path = require('path');
const fs = require('fs');
const appDirectory = fs.realpathSync(process.cwd());

const moduleFileExtensions = [
  'web.mjs',
  'mjs',
  'web.js',
  'js',
  'web.ts',
  'ts',
  'web.tsx',
  'tsx',
  'json',
  'web.jsx',
  'jsx',
];
const resolveModule = (resolveFn, filePath) => {
  const extension = moduleFileExtensions.find((v) => fs.existsSync(resolveFn(`${filePath}.${v}`)));

  if (extension) {
    return resolveFn(`${filePath}.${extension}`);
  }

  return resolveFn(`${filePath}.js`);
};
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

module.exports = {
  dotenv: resolveApp('.env'),
  appPath: resolveApp('.'),
  appBuild: resolveApp('build'),
  appDist: resolveApp('dist'),
  appIndexJs: resolveModule(resolveApp, 'src/index'),
  appIndexStyle: resolveApp('src/assets/styles/app.scss'),
  appPackageJson: resolveApp('package.json'),
  appSrc: resolveApp('./src'),
  appAPI: resolveApp('src/api'),
  appAssets: resolveApp('src/assets'),
  appTsConfig: resolveApp('tsconfig.json'),
  appJsConfig: resolveApp('jsconfig.json'),
  yarnLockFile: resolveApp('yarn.lock'),
  testsSetup: resolveModule(resolveApp, 'src/setupTests'),
  proxySetup: resolveApp('src/setupProxy.js'),
  appNodeModules: resolveApp('node_modules'),
};